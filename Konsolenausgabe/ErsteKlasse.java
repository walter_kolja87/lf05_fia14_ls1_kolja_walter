import java.util.Scanner;

public class ErsteKlasse {

	public static void main(String[] args) {
		System.out.println("Das ist ein \"Beispielsatz\"\nEin Beispielsatz ist das.");
String name = "Kolja";
int alter = 34;
		System.out.println("Ich heiße "+ name + " und bin " + alter + " Jahre alt.");
	// Println erzeugt einen Zeilenumbruch.;
	
		String s = "*";
		System.out.printf( "%7s\n", s);//Baumdiagramm Zeile1;
		String b = "***";
		System.out.printf( "%8s\n", b);//Baumdiagramm Zeile2;
		String a = "*****";
		System.out.printf( "%9s\n", a);//Baumdiagramm Zeile3;
		String b1 = "*******";
		System.out.printf( "%10s\n", b1);//Baumdiagramm Zeile4;
		String a1 = "*********";
		System.out.printf( "%11s\n", a1);//Baumdiagramm Zeile5;
	    String b2 = "***********";
	    System.out.printf( "%12s\n",b2);//Baumdiagramm Zeile6;
	    String a2 = "***";
	    System.out.printf( "%8s\n", a2);//Baumdiagramm Zeile6;
	    String b3 = "***";
	    System.out.printf( "%8s\n", b3);//Baumdiagramm Zeile6;
	    
	    System.out.printf("%.2f\n", 22.4234234);
		System.out.printf("%.2f\n", 111.2222);
		System.out.printf("%.2f\n", 4.0);
		System.out.printf("%.2f\n", 1000000.551);
		System.out.printf("%.2f\n", 97.34);
		System.out.println("");
		
		System.out.printf("%-5s = ", "!0");
		System.out.printf("%-19s = ", "");
		System.out.printf("%4s\n", "1");
		System.out.printf("%-5s = ", "!1");
		System.out.printf("%-19s = ", "1");
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s = ", "!2");
		System.out.printf("%-19s = ", "1 * 2");
		System.out.printf("%4s\n", "2");
		
		System.out.printf("%-5s = ", "!3");
		System.out.printf("%-19s = ", "1 * 2 * 3");
		System.out.printf("%4s\n", "6");
		
		System.out.printf("%-5s = ", "!4");
		System.out.printf("%-19s = ", "1 * 2 * 3 * 4");
		System.out.printf("%4s\n", "24");
		
		System.out.printf("%-5s = ", "!5");
		System.out.printf("%-19s = ", "1 * 2 * 3 * 4 * 5");
		System.out.printf("%4s\n", "120");
		System.out.println("");
		
		System.out.printf("%5s\n", "**");
		System.out.print("*");
		System.out.printf("%7s\n", "*");
		System.out.print("*");
		System.out.printf("%7s\n", "*");
		System.out.printf("%5s\n", "**");
		System.out.println("");
		
		System.out.printf("%-12s|", "Fahrenheit");
		System.out.printf("%10s\n", "Celsius");
		System.out.println("-----------------------");
		
		System.out.printf("%-12s|", "-20");
		System.out.printf("%10.2f\n", -28.8889);
		
		System.out.printf("%-12s|", "-10");
		System.out.printf("%10.2f\n", -23.3333);
		
		System.out.printf("%-12s|", "+0");
		System.out.printf("%10.2f\n", -17.7778);
		
		System.out.printf("%-12s|", "+20");
		System.out.printf("%10.2f\n", -6.6667);
		
		System.out.printf("%-12s|", "+30");
		System.out.printf("%10.2f\n", -1.1111);
		
		Scanner scanner = new Scanner(System.in);
		int zahl1 = scanner.nextInt();
		int zahl2 = scanner.nextInt();
		int zahl3 = scanner.nextInt();
		if(zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.print("Zahl 1 ist am gr��ten");
		} else if (zahl2 > zahl1 && zahl2 > zahl3) {
			System.out.print("Zahl 2 ist am gr��ten");
		} else if (zahl3 > zahl1 && zahl3 > zahl2) {
			System.out.print("Zahl 3 ist am gr��ten");
		} else {
			System.out.print("Zahlen sind gleich gro�");
			
		}}}
	

