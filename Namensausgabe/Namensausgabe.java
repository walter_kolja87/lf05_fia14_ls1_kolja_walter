
		import java.util.Scanner; // Import der Klasse Scanner 
		 
		public class Namensausgabe  
		{ 
		   
		  public static void main(String[] args) // Hier startet das Programm 
		  { 
		     
		    // Neues Scanner-Objekt myScanner wird erstellt     
		    Scanner myScanner = new Scanner(System.in);  
		     
		    System.out.print("Bitte geben Sie ihren Vornamen ein: ");    
		     
		    // Die Variable zahl1 speichert die erste Eingabe 
		    int name1 = myScanner.nextInt();  
		     
		    System.out.print("Bitte geben Sie Ihren Nachnamen ein: "); 
		     
		    // Die Variable zahl2 speichert die zweite Eingabe 
		    int name2 = myScanner.nextInt();  
		     
		    // Die Addition der Variablen zahl1 und zahl2  
		    // wird der Variable ergebnis zugewiesen. 
		    int ergebnis = name1 + name2;  
		     
		    System.out.print("\n\n\nHallo, Ihr Name lautet: "); 
		    System.out.print(ergebnis);   
		 
		    myScanner.close(); 
		     
		  }    
		}
	}

}
