import java.util.Scanner; // Import der Klasse Scanner 
 
public class Konsoleneingabe  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner wetter = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie an ob die Sonne scheint: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    
    
    String eingabewetter = wetter.next();
     
    if (eingabewetter.charAt(0)=='j') {
    System.out.print("Es regnet nicht");
    }
    
    else  
    System.out.print("Dann regnet es sehr wahrscheinlich");
     
    
 
    wetter.close(); 
     
  }    
}